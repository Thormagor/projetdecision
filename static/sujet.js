"use strict";


function initSujets() {
    const subjects = document.getElementById("sujets");

    const liste = document.createElement('ul');
    liste.id = "liste";
    subjects.appendChild(liste);

    let bouton = document.createElement('button');
    bouton.innerText = "Créer sujet";
    bouton.onclick = addSubject;
    subjects.appendChild(bouton);
    fetch("/sujet/").then((raw)=>raw.json().then(rempliSujets));
}

/*
    listeDico exemple :
    [{titre : "exTitre", id : "exId", "description": "machin"},
    {titre : "titre2", id : "id2", "description": "bidule"}];
*/  
function rempliSujets(listeDico) {
    for (const child of document.getElementById('liste').children) {
        child.remove();
    }
    for ( const sujet of listeDico) {
        const sub = document.createElement('li');
        sub.innerText = sujet["titre"];
        sub.onclick = ()=>choixSujet(sujet.id);
        sub.id = sujet["id"];
        document.getElementById('liste').appendChild(sub);
    }
}



function addSubject() {
    document.createElement("li");

    const nomInput =  prompt('nom du sujet');
    

    if ( nomInput != null && nomInput != "") {
        let descInput =  prompt("description du sujet");
        if (descInput != null && descInput != "") {
            const dico = {
                'titre' : nomInput,
                'desc' : descInput
            };
            console.log(dico);
        } else {
            console.log("description is null");
        }
    } else {
        console.log('titre is null');
    }

}

function choixSujet(id){
    clearMessages();
    clearSondages();
    fetch("/sujet/"+id).then((raw)=>raw.json().then(afficheSujet));
}
function afficheSujet(sujet){
    for(const id_message of sujet.messages)
        initMessage(id_message);
    for(const id_sondage of sujet.sondages)
        initSondage(id_sondage);
}

initSujets();
